﻿using System;

namespace exercise_13
{
    class Program
    {
        static void Main(string[] args)
        {
        //Clear noise    
        Console.Clear();

        //Declare variable
        var nameFirst = "Phoebe";

        //Display var nameFirst to screen
        Console.WriteLine($"Hello my name is {nameFirst}!");

        Console.ReadKey();
        }
    }
}
